# -*- coding: utf-8 -*-
# Build to assist Applicants in the Python course

import re
import datetime
import pickle
import os

class Diksyone:
    # Diksyone (Non):
    # Se yon liv ki itilize epi koleskyone tout enfomasyon sou tout ki itilize nan yon lang
    # Li plis ke enpotan
    
    def __init__(self):
        self.db = None
        self.root = os.getcwd() # root directory
        self.display_format = "%s\n%s - %s:(%s)\n%s\n\n"

    
    def con_db(self,mode='ab'):
        # Connection to db, it could be MySQL, PostgreSQL, whatever.
        # The connection should be handled here
        db_name = 'kreyol.txt' # filename
        fileName = self.get_directory() + db_name
        if not os.path.exists(fileName):
            with open(fileName,'w'):
                pass
        self.db = open(fileName,mode)
        return self.db

    def get_directory(self):
        # merge root directory with actual custom directory
        folder = '/diksyone_kreyol/'
        dirName = self.root + folder
        if not os.path.exists(dirName):
            os.mkdir(dirName)
        return dirName

    def add_word(self,word=None,cat=None,defition=None):
        date = datetime.datetime.now()
        if not word:
            word = input("Antre mo w ap anrejistre nan diksyonè a: ")
        if not cat:
            cat = input("Chwazi sa mo a ye:\nNon[1]\nAdjektiv[2]Vèb[3]\nPwonon[4]\n: ")
            if cat == '1':
                cat = 'Non'
            elif cat == '2':
                cat = 'Adjektif'
            elif cat == '3':
                cat = 'Vèb'
            elif cat == '4':
                cat = 'Pwonon'
        if not defition:
            defition = input("Tape definisyon an: ")
        row = {'date_created':date,'word':word,'cat':cat,'def':defition}
        self.add_row(row)

    def get_data(self):
        try:
            # Connect to db with read mode
            self.db = self.con_db(mode='rb')
            data = pickle.load(self.db) # load data with PICKLE module
        except EOFError:
            data = []
        return data

    def update_data(self,data):
        # update whole database 
        db = self.con_db(mode='wb') # Connect to db with writing mode
        pickle.dump(data,db)
        db.close()

    def add_row(self,row):
        # add new entry in database with new ID
        data = self.get_data()
        row['id'] = max(data, key=lambda x:x['id'])['id'] + 1 if data else 1
        data.append(row)
        self.update_data(data)
        return row

    def get_row(self,id):
        # Retrieve data, like SQL Query
        data = self.get_data()
        for item in data :
            if item['id'] == id:
                return self.render(item)

    def update_row(self,id,**kwargs):
        # update an instance query with ID
        data = self.get_data()
        for item in data :
            if item['id'] == id:
                if 'word' in kwargs:
                    item['word'] = kwargs.get('word')
                if 'cat' in kwargs:
                    item['cat'] = kwargs.get('cat')
                if 'definition' in kwargs:
                    item['def'] = kwargs.get('definition')

                self.update_data(data)
                break


    def delete_row(self,id):
        # delete an instance query with ID
        data = self.get_data()
        for index,item in enumerate(data):
            if item['id'] == id:
                data.pop(index)
                self.update_data(data)
                break

    def render(self,row):
        # Display mode: display instance
        print(self.display_format % (row.get('date_created').strftime('%d-%m-%Y %H:%M'),
                                     row.get('id'),row.get('word'),row.get('cat'),row.get('def')))

    def search(self,word):
        # Search system by words
        output_data = []
        data = self.get_data()
        for item in data :
            result = re.search(item['word'], word)
            if result:
                output_data.append(item)
        return self.display(data=output_data)
        
    def display(self,data=None):
        # [{'word':<word>,'cat':<cat>,'def':<def>}]
        if not data:
            data = self.get_data()
        if not data:
            print("Diksyonè a vid")
            choice = input("Peze 'a' si w vle ajoute mo ladan, oubyen nenpot lòt touch pou w inyore: ")
            if choice.lower() == 'a':
                self.add_word()
            return None
        for item in data:
            self.render(item)


if __name__ == '__main__':
    d = Diksyone()
